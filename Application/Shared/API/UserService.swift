//
//  UserService.swift
//  Monumento
//
//  Created by Aaryan Kothari on 09/07/21.
//

import Firebase
import Combine


class UserService: ObservableObject {
    private let userCollection = Firestore.firestore().collection("users")
    
    let uid = Auth.auth().currentUser?.uid ?? "uid"
    private var cancellables: [AnyCancellable] = []
    
    let onErrorCompletion: ((Subscribers.Completion<Error>) -> Void) = { completion in
            switch completion {
            case .finished: print("🏁 finished")
            case .failure(let error): print("❗️ failure: \(error)")
            }
        }
    
    let onValue = { print("✅ value") }
    
    func addUser(_ user: User) {
        userCollection
            .document(uid)
            .setData(user.asDictionary)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: onValue)
            .store(in: &cancellables)
    }
    
    func checkUser(id:String,completion: @escaping (Bool) -> ()) {
        userCollection
            .document(id)
            .getDocument(as: User.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { completion($0 == nil) } )
            .store(in: &cancellables)
    }
    
    func fetchUser(for uid: String,completion: @escaping (User?) -> ()) {
        userCollection
            .document(uid)
            .getDocument(as: User.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: {  completion($0) })
            .store(in: &cancellables)
    }
    
    func followUser(with uid: String) {
        guard let myUid = User.current.uid else {
            print("user uid not found")
            return
        }
        
        userCollection
            .document(myUid)
            .updateData(["following":FieldValue.arrayUnion([uid])])
            .sink(receiveCompletion: onErrorCompletion, receiveValue: onValue)
            .store(in: &cancellables)
    }
    
    
    func unFollowUser(with uid: String) {
        guard let myUid = Auth.auth().currentUser?.uid else {
            print("user uid not found")
            return
        }
        
        userCollection
            .document(myUid)
            .updateData(["following":FieldValue.arrayRemove([uid])])
            .sink(receiveCompletion: onErrorCompletion, receiveValue: onValue)
            .store(in: &cancellables)
    }
    
    func queryUsers(for userName: String,completion: @escaping ([User]?) -> ()) {
        userCollection.whereField("searchParams", arrayContains: userName)
            .getDocuments(as: User.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: {  print($0);completion($0) })
            .store(in: &cancellables)
    }
    
}
