//
//  UserModel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 09/07/21.
//

import Foundation


struct Session {
    var uid: String?
    var email: String?
    
    init(uid: String? = nil, email: String? = nil) {
        self.uid = uid
        self.email = email
    }
}
