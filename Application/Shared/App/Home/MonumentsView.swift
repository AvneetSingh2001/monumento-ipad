//
//  MonumentsView.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import SwiftUI

struct MonumentsView: View {
    @StateObject var monumentVM: MonumentViewModel = MonumentViewModel()
    @State var popularOn: Bool = true
    
    
    var body: some View {
        NavigationView {
        VStack {
            MonumentoHeader()
            
            DetectMonumentButton().button {
                self.monumentVM.showImagePicker()
            }
            
            PopularToggle(monumentVM: monumentVM)
            
            MonumentList(monumentVM: monumentVM)
            
            NavigationLink(destination: MonumentDetailView(monument: monumentVM.monument), tag: "ABC", selection: $monumentVM.selection) { EmptyView()
            }
            Spacer()
        }
        .padding(.horizontal,45)
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .sheet(isPresented: $monumentVM.showingImagePicker,onDismiss: monumentVM.loadImage) {
            ImagePicker(image: $monumentVM.inputImage, source: .photoLibrary)
        }
    }
}

struct MonumentsView_Previews: PreviewProvider {
    static var previews: some View {
        MonumentsView()
    }
}
