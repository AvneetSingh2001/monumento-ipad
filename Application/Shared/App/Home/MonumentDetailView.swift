//
//  MonumentDetailView.swift
//  Monumento
//
//  Created by Aaryan Kothari on 16/07/21.
//

import SwiftUI
import MapKit

struct MonumentDetailView: View {
    var monument: Monument
    let alignment = Alignment(horizontal: .center, vertical: .bottom)
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    
    var body: some View {
        GeometryReader { gr in
        VStack {
            HStack {
            BackButton(title: monument.name ?? "Monumento")
                .button { self.mode.wrappedValue.dismiss() }
                .navigationBarBackButtonHidden(true)
             
                AugmentButton()
            }
            
            Map(coordinateRegion: $region)
                .frame(height: gr.size.height / 3)
            CircularImage(url: monument.image,height: gr.size.width/4,image: monument.uiImage)
                .offset(y: -gr.size.width/8)
                .padding(.bottom,-gr.size.width/8)
                .zIndex(1)
            
                WebView(url: monument.wikiUrl)
            }
        }
        .onAppear(perform: setMap)
    }
    
    func setMap() {
        self.region.center = CLLocationCoordinate2D(latitude: monument.coordinate[0], longitude: monument.coordinate[1])
    }
}

struct MonumentDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MonumentDetailView(monument: Monument.data[0])
    }
}
