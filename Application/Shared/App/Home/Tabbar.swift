//
//  Tabbar.swift
//  Monumento
//
//  Created by Aaryan Kothari on 06/07/21.
//

import SwiftUI

struct Tabbar : View {
    @State private var selectedIndex: Int = 0
    @State private var orientation = UIDeviceOrientation.portrait
    
    var body: some View {
        Group {
            if orientation.isPortrait {
                TabView(selection: $selectedIndex) {
                    MonumentsView()
                        .tabItem {
                            Label("Home", systemImage: "house.fill")
                        }
                        .tag(0)
                    
                    DiscoverView()
                        .tabItem {
                            Label("Discover", systemImage: "safari")
                        }
                        .tag(1)
                    
                    FeedView()
                        .tabItem {
                            Label("Feed", systemImage: "heart")
                        }
                        .tag(2)
                    
                    ProfileView(profileVM: ProfileViewModel(uid: User.current.uid ?? ""))
                        .tabItem {
                            Label("Profile", systemImage: "person.fill")
                        }
                        .tag(3)
                }
            } else if orientation.isLandscape {
                NavigationView {
                    List {
                        NavigationLink(destination: MonumentsView()) {
                            Label("Home", systemImage: "house.fill")
                        }
                        
                        NavigationLink(destination: DiscoverView()) {
                            Label("Feed", systemImage: "heart")
                        }
                        
                        NavigationLink(destination: Text("ACTIVITY")) {
                            Label("Discover", systemImage: "safari")
                        }
                        
                        NavigationLink(destination: ProfileView(profileVM: ProfileViewModel(uid: User.current.uid ?? ""))) {
                            Label("Profile", systemImage: "person.fill")
                        }
                    }
                    .listStyle(SidebarListStyle())
                }
            } else if orientation.isFlat {
                Text("Flat")
            } else {
                Text("Unknown")
            }
        }
        .onRotate { newOrientation in
            orientation = newOrientation
        }
    }
}

struct Tabbar_Previews: PreviewProvider {
    static var previews: some View {
        Tabbar()
    }
}

struct DeviceRotationViewModifier: ViewModifier {
    let action: (UIDeviceOrientation) -> Void
    
    func body(content: Content) -> some View {
        content
            .onAppear()
            .onReceive(NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)) { _ in
                action(UIDevice.current.orientation)
            }
    }
}

// A View wrapper to make the modifier easier to use
extension View {
    func onRotate(perform action: @escaping (UIDeviceOrientation) -> Void) -> some View {
        self.modifier(DeviceRotationViewModifier(action: action))
    }
}
