//
//  UserForm.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 08/07/21.
//

import SwiftUI

struct UserForm: View {
    @ObservedObject var userVM: UserViewModel
    @State var profilePicture: UIImage? = nil
    
    var gradient: Gradient = Gradient(colors: [.customYellow,.customOrange])
    var body: some View {
        GeometryReader { gr in
            ZStack {
                gradient.topBottomLinear
                VStack(spacing:25) {
                    CrossButton()
                    AKImage(inputImage: $profilePicture,gradient: gradient)
                        .frame(width: gr.size.width * 0.33)
                        .padding(.bottom,35)
                    
                    HStack(spacing:25) {
                        
                        CustomTextField(type: .fName, text: $userVM.firstName)
                        
                        CustomTextField(type: .lName, text: $userVM.lastName)
                        
                    }
                    
                    CustomTextField(type: .username, text: $userVM.username)

                    CustomTextField(type: .status, text: $userVM.status)
                    
                    CustomButton(title: userVM.buttonTitle, action: userVM.addUser)
                                        
                    PrivacyPolicyLabel()
                }
                .padding(33)
                .background(Color.white)
                .cornerRadius(22)
                .padding(125)
            }
            .edgesIgnoringSafeArea(.all)
        }
    }
}

struct UserForm_Previews: PreviewProvider {
    static var previews: some View {
        UserForm(userVM: UserViewModel())
    }
}
