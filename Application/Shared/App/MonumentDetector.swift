//
//  MonumentDetector.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 09/08/21.
//

import UIKit
import Alamofire

class MonumentDetector {
    
    var base64encodedImage: String?
    var uiImage: UIImage?
    
    init() { }
    
    init(image:UIImage?) {
        self.uiImage = image
        self.base64encodedImage = image?.jpegData(compressionQuality: 0.3)?.base64EncodedString()
    }
    
    func detect(completion:@escaping(Monument)->()) {
        
        guard let image = base64encodedImage else { return }
        let url = URL(string: "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyB3Rtf6kKP2zzs9HN3zzpJMu1oUtDYk4_4")!
        let body = CloudVisionAPI.Request(with: image)
        
        AF.request(url,method: .post,parameters: body,encoder: JSONParameterEncoder.default)
            .responseDecodable(of: CloudVisionAPI.Response.self) { res in
                switch res.result {
                case .success(let response):
                    print("success")
                    print(response)
                    guard let data = response.responses?.first?.landmarkAnnotations?.first else {
                        print("ERROR DECODING")
                        return
                    }
                    let title = data.description?.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "_") ?? "monuments"
                    var monument = Monument(id: UUID(), name: data.description, coordinate: data.coordinates, imageUrlString: "", wikiLink: "https://en.wikipedia.org/wiki/\(title)", isPopular: false)
                    monument.uiImage = self.uiImage
                    completion(monument)
                case .failure(let error):
                    print(error)
                }
            }
    }
}
