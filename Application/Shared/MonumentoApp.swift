//
//  MonumentoApp.swift
//  Shared
//
//  Created by Aaryan Kothari on 22/06/21.
//

import SwiftUI

@main
struct MonumentoApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ARView()
        }
    }
}

