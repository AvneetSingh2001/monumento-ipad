//
//  Gradient.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import Foundation
import SwiftUI

extension Gradient {
    var topBottomLinear: LinearGradient {
        return LinearGradient(gradient: self, startPoint: .top, endPoint: .bottom)
    }
}
