//
//  JSON.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 01/08/21.
//

import Foundation

struct JSON {
    static let encoder = JSONEncoder()
}
extension Encodable {
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
