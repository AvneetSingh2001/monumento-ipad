//
//  Color.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/06/21.
//

import SwiftUI

extension Color {
    static let customYellow = Color(UIColor(named: "MonumentoYellow")!)
    static let customOrange = Color(UIColor(named: "MonumentoOrange")!)
    static let lightGreen = Color(UIColor(named: "MonumentoLightGreen")!)
    static let darkGreen = Color(UIColor(named: "MonumentoDarkGreen")!)
    static let lightBlue = Color(UIColor(named: "MonumentoLightBlue")!)
    static let darkBlue = Color(UIColor(named: "MonumentoDarkBlue")!)
    static let lightPink = Color(UIColor(named: "MonumentoLightPink")!)
    static let darkPink = Color(UIColor(named: "MonumentoDarkPink")!)
}
