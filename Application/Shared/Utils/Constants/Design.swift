//
//  DesignConstants.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import Foundation
import UIKit

struct DesignConstants {
    struct Login {
        static let horizontal: CGFloat = UIDevice.isIPad ? 125 : 0
    }
}
