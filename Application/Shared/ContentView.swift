//
//  ContentView.swift
//  Shared
//
//  Created by Aaryan Kothari on 22/06/21.
//

import SwiftUI

struct ContentView: View {
    @StateObject var listener: AuthListener = AuthListener()
    @StateObject var userVM: UserViewModel = UserViewModel()
    
    @AppStorage("userExists") var userExists: Bool = false
    var body: some View {
        Group {
            if listener.session == nil {
                AuthView()
            } else if !userVM.userCreated{
                UserForm(userVM: userVM)
            } else {
                Tabbar()
            }
        }
        .onAppear(perform: listener.listen)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
