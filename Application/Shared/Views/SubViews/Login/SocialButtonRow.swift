//
//  SocialButtonRow.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct SocialButtonRow: View {
    
    // MARK: - PROPERTIES
    @StateObject var session: SessionStore
    let googleVM : GoogleCoordinator
    let appleVM : AppleSignInCoordinator
    let columns = [GridItem(.adaptive(minimum: 215, maximum: 400),spacing: 35)]
    
    // MARK: - INITIALIZE
    init(session: SessionStore) {
        self.appleVM = AppleSignInCoordinator(session: session)
        self.googleVM = GoogleCoordinator(session: session)
        _session = StateObject(wrappedValue: session)
    }
    
    // MARK: - BODY
    var body: some View {
        LazyVGrid(columns: columns, spacing: 10) {
            Button(action: appleVM.signinWithApple){ }
                .buttonStyle(SocialButtonStyle(type: .apple))
                .frame(height: 52)
            Button(action: googleVM.googleSignin){ }
                .buttonStyle(SocialButtonStyle(type: .google))
                .frame(height: 52)
        } // LAZYGRID
    }
}

// MARK: - PREVIEW
struct SocialButtonRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SocialButtonRow(session: SessionStore(mode: .login))
                .previewDevice("iPad Pro (11-inch) (3rd generation)")
           SocialButtonRow(session: SessionStore(mode: .login))
                .previewDevice("iPhone 12")
        }
    }
}
