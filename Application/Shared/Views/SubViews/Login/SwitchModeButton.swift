//
//  SwitchModeButton.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct SwitchModeButton: View {
    @StateObject var session: SessionStore
    var body: some View {
            HStack {
                Spacer()
                Text(session.switchModeMessage)
                    .italic()
                Text(session.screenTitle)
                    .foregroundColor(session.primaryColor)
                    .italic()
                Spacer()
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct SwitchModeButton_Previews: PreviewProvider {
    static var previews: some View {
        SwitchModeButton(session: SessionStore(mode: .login))
    }
}
