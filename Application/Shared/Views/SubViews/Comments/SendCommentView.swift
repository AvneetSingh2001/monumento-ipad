//
//  SendCommentView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 25/07/21.
//

import SwiftUI
import Kingfisher

struct SendCommentView: View {
    @ObservedObject var postVM: PostViewModel
    var body: some View {
            ZStack(alignment: Alignment(horizontal: .trailing, vertical: .center)) {
                HStack {
                KFImage(User.current.profilePictureUrl)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
                Spacer()
                }
                .padding(10)
            TextField("Add a Comment", text: $postVM.comment)
                .padding()
                .textFieldStyle(PlainTextFieldStyle())
                .padding(.leading,60)
            Button("Post",action: postVM.addComment)
            .disabled(!postVM.enabled)
                .padding(.trailing,25)
        }
            .frame(height: 70)
            .overlay(Capsule().stroke(Color.customOrange,lineWidth: 2))
    }
}

struct SendCommentView_Previews: PreviewProvider {
    static var previews: some View {
        SendCommentView(postVM: PostViewModel(with: ""))
    }
}
