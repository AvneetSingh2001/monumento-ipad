//
//  CommentRow.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 24/07/21.
//

import SwiftUI
import Kingfisher

struct CommentRow: View {
    var comment: Comment
    var body: some View {
        HStack(alignment:.top) {
            KFImage(comment.author?.profilePictureUrl)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .frame(width: 60, height: 60, alignment: .center)
                .clipShape(Circle())
            Text("@\(comment.author?.username ?? "username")").bold() + Text(" " + (comment.comment ?? ""))
            Spacer()
        }
        .font(.system(size: 25))
        .padding(.bottom,20)
    }
}

struct CommentRow_Previews: PreviewProvider {
    static var previews: some View {
        CommentRow(comment: Comment.data)
    }
}
