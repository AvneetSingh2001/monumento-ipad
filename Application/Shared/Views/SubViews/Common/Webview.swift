//
//  Webview.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 17/07/21.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    
    let url : URL?
    
    init(url:URL?) {
        self.url = url
    }
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let url = self.url else {
            print("INVALID URL")
            return
        }
        let request = URLRequest(url: url)
        uiView.load(request)
    }
}
