//
//  DiscoverROw.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 13/08/21.
//

import SwiftUI
import Kingfisher

struct DiscoverRow: View {
    var user: User
    var body: some View {
        HStack(spacing:15) {
            KFImage(user.profilePictureUrl)
                .resizable()
                .aspectRatio(1,contentMode: .fit)
                .clipShape(Circle())
            
            Text(user.name ?? "")
                .font(.system(size: 24, weight: .medium))
                .foregroundColor(.black)
            Spacer()
        }
        .padding()
        .frame(height:85)
        .roundedBorder(radius: 8, color: .black)
    }
}

struct DiscoverRow_Previews: PreviewProvider {
    static var previews: some View {
        DiscoverRow(user: User.current)
    }
}
