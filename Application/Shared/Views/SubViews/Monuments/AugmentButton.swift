//
//  AugmentButton.swift
//  Monumento
//
//  Created by Aaryan Kothari on 18/07/21.
//

import SwiftUI

struct AugmentButton: View {
    var body: some View {
        HStack {
            Spacer()
            Image(systemName:"move.3d")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .foregroundColor(.white)
                .shadow(color: .black, radius: 2)
                .padding(3)
            Text("AUGMENT")
                .font(.system(size: 30,weight: .bold))
                .foregroundColor(.white)
                .shadow(color: .black, radius: 2)
            Spacer()
        }
        .padding(12)
        .background(Color.customYellow)
        .roundedBorder(radius: 8, color: .black)
        .aspectRatio(5, contentMode: .fit)
        .frame(maxHeight:55)
        .padding(.trailing,16)
    }
}

struct AugmentButton_Previews: PreviewProvider {
    static var previews: some View {
        AugmentButton()
    }
}
