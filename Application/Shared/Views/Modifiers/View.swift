//
//  View.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct RounderBorder: ViewModifier {
    var radius: CGFloat
    var color: Color
    func body(content: Content) -> some View {
        content
            .overlay(
                RoundedRectangle(cornerRadius: radius)
                    .stroke(color, lineWidth: 2)
            )
    }
}


extension View {
    func roundedBorder(radius: CGFloat = 4, color: Color = Color.gray) -> some View {
        self.modifier(RounderBorder(radius: radius, color: color))
    }
}
