//
//  Button.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 01/07/21.
//

import SwiftUI

struct ButtonConverter: ViewModifier {
    var action: () -> Void
    
    func body(content: Content) -> some View {
        Button(action:action) {
            content
        }
        .buttonStyle(PlainButtonStyle())
    }
}


extension View {
    func button(action:@escaping () -> Void) -> some View {
        self.modifier(ButtonConverter(action:action))
    }
}
