//
//  Button.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct CustomButtonStyle: ButtonStyle {
    var gradient: Gradient
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            Spacer()
            configuration.label
                .font(.system(size: 28, weight: .black))
                .foregroundColor(.white)
            Spacer()
        }
        .padding(10)
        .aspectRatio(6, contentMode: .fit)
        .background(LinearGradient(gradient: gradient, startPoint: .trailing, endPoint: .leading))
        .cornerRadius(8)
    }
}

struct SocialButtonStyle: ButtonStyle {
    var type: SocialButtonType
    
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            Spacer()
            Image(type.icon)
                .resizable()
                .scaledToFit()
                .padding(.vertical, 16)
            Text(type.title).font(.system(size: 17))
            Spacer()
        }
        .roundedBorder(radius: 8, color: .black)
    }
}

struct CustomButton: View {
    var title: String
    var action: ()->(Void) = { }
    var gradient: Gradient = Gradient(colors: [.customYellow,.customOrange])
    
    var body: some View {
        Button(title, action: action)
            .buttonStyle(CustomButtonStyle(gradient: gradient))
    }
}

