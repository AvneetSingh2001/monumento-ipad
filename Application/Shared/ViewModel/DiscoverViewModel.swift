//
//  DiscoverViewModel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 13/08/21.
//

import UIKit
import SwiftUI
import Firebase

class DiscoverViewModel: ObservableObject {
    @Published var userService: UserService = UserService()
    @Published var users: [User] = []
    @Published var searchUsers: [User] = []
    @Published var query: String = "" {
        didSet {
            self.getQueryUsers()
        }
    }
    
    init() {
        self.getDefaultUsers()
    }
    
    func getDefaultUsers() {
        
    }
    
    func getQueryUsers() {
        userService.queryUsers(for: query.lowercased()) { self.searchUsers = $0 ?? [] }
    }
}

