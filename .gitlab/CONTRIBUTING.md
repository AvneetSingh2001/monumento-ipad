# Contributing to the project

All contributions to the project should follow the following procedure:

## Table of Contents

- [Installation](#installation)
- [Creating an issue](#creating-an-issue)
- [Creating a merge request](#creating-a-merge-request)
- [Set up instructions](#set-up-instructions)
- [Best practices](#best-practices)
- [Some don'ts](#some-donts)

## Installation

Make sure you have the latest version of [Xcode (v 12.5)](https://developer.apple.com/xcode/resources/) installed.

## Creating an issue

If you find a bug in this project, have an amazing feature request or just have a doubt about the project – **create an issue!**

For creating an issue, it is very **important** to follow the [best practices](#best-practices) and [some don'ts](#some-donts)

## Creating a Merge Request

If you're willing to patch a bug or add a feature, you can request for the same by commenting in the respective issue. A maintainer will assign the issue to you, post which you can work on the issue and submit a merge request.

Once you’ve opened a merge request a discussion will start around your proposed changes. Other contributors and users may comment on the merge request, but ultimately the decision is made by the maintainer(s).

For creating a merge request, it is very **important** to follow the [best practices](#best-practices) and [some don'ts](#some-donts)

## Set up instructions

1. [Fork this repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
2. Clone the fork on your system
3. Sync your fork with 'upstream' by following these [instructions](#sync-instructions-local-repository-with-upstream)
4. [Create a new branch](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) for the feature you will be working on.
5. You are good to go!

## Sync Instructions local repository with upstream

1. [Connect your local to the original ‘upstream’ repository by adding it as a remote.](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)
2. Pull in changes from ‘upstream’ often so that you stay up to date so that when you submit your merge request, merge conflicts will be less likely.

## Best practices

1. Try to follow consistent syntax while coding. ( [guide for reference](https://github.com/raywenderlich/swift-style-guide) )
2. Reference the issue being fixed in the corresponding MR.
3. Use meaningful commit messages in a MR.
4. Use one commit per task. Do not over commit (add unnecessary commits for a single task) or under commit (merge 2 or more tasks in one commit).
5. Add screenshot/short video in case the changes made in the MR, are being reflected in the UI of the application.
  
## Some don'ts

**DO NOT**
1. Send a MR without an existing issue.
2. Fix an issue assigned to somebody else and submit a MR before the assignee does.
3. Report issues which are previously reported by others. (Please check both the open and closed issues).
4. Suggest unnecessary or completely new features in the issue list.
5. Add unnecessary spacing or indentation to the code.

<hr>

### If you face **any** problems, feel free to ask our community at [Gitter](https://gitter.im/AOSSIE/Monumento)

<hr>
